#include "HelloWorldScene.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
	if (!CCLayer::init())
	{
		return false;
	}
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
	

	tmx_map = TMXTiledMap::create("mapPac.tmx");
	this->addChild(tmx_map, 0, 1);
	//////////////////////////////////////////////////////////////////////////

	_meta = tmx_map->layerNamed("Meta");
	_meta->setVisible(false);
	//////////////////////////////////////////////////////////////////////////


	foodObject = tmx_map->objectGroupNamed("FoodObject");
	ValueVector foodVector = foodObject->getObjects();
	for (int i = 0; i < foodVector.size(); ++i){
		ValueMap foodValue = (foodVector[i]).asValueMap();
		int x = foodValue["x"].asInt();
		int y = foodValue["y"].asInt();
		banana = Sprite::create("banana.png",Rect(0,0,32,32));
		banana->setPosition(Point(x + 16, y + 16));
		this->addChild(banana);
		vtBanana.push_back(banana);
	}
	

	//////////////////////////////////////////////////////////////////////////
	mainObject = tmx_map->objectGroupNamed("MainObject");
	spawnPoint = mainObject->getObject("SpawnPoint");
	xPos = spawnPoint["x"].asInt();
	yPos = spawnPoint["y"].asInt();
	pacman = Sprite::createWithSpriteFrameName("pacman1.png");
	pacman->setPosition(Point(xPos ,yPos));
	this->addChild(pacman);

	Vector<SpriteFrame*> pacFrame(2);
	pacFrame.pushBack(SpriteFrameCache::sharedSpriteFrameCache()->getSpriteFrameByName("pacman1.png"));
	pacFrame.pushBack(SpriteFrameCache::sharedSpriteFrameCache()->getSpriteFrameByName("pacman2.png"));
	Animation *animation = Animation::createWithSpriteFrames(pacFrame, 0.3f);
	Animate *animate = Animate::create(animation);
	pacman->runAction(RepeatForever::create(animate));
	//
	//
	EventListenerKeyboard* listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(HelloWorld::keyPressed, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	//////////////////////////////////////////////////////////////////////////

	score = 0;
	auto label = LabelTTF::create("My Score: ", "Arial", 30);
	label->setPosition(750, 450);
	this->addChild(label);
	scoreLabel = LabelTTF::create(std::to_string(score), "Arial", 50);
	scoreLabel->setPosition(750, 400);
	this->addChild(scoreLabel);

  
	
	//////////////////////////////////////////////////////////////////////////
    return true;
}
void HelloWorld::keyPressed(EventKeyboard::KeyCode keyCode, Event *event){
	
		if (keyCode == EventKeyboard::KeyCode::KEY_A || keyCode == EventKeyboard::KeyCode::KEY_LEFT_ARROW){
			

				auto actionRotation = RotateTo::create(0.1f, 180);
				auto action = MoveBy::create(0.2, Point(-32, 0));
				
				if (checkCollision(Point(xPos-32,yPos)) == false)
				{
					xPos -= 32;
					pacman->runAction(Sequence::create(actionRotation, action, CallFunc::create(CC_CALLBACK_0(HelloWorld::checkCollisonBanana, this)), NULL));
				}
				else{
					
					return;
				}
			
		}
		if (keyCode == EventKeyboard::KeyCode::KEY_D || keyCode == EventKeyboard::KeyCode::KEY_RIGHT_ARROW){
			auto actionRotation = RotateTo::create(0.1f, 0);
			auto action = MoveBy::create(0.2, Point(32, 0));
			
			if (checkCollision(Point(xPos + 32, yPos)) == false)
			{
				xPos += 32;
				pacman->runAction(Sequence::create(actionRotation, action, CallFunc::create(CC_CALLBACK_0(HelloWorld::checkCollisonBanana, this)), NULL));
			}
			else
			{
				return;
			}

		}
		if (keyCode == EventKeyboard::KeyCode::KEY_W || keyCode == EventKeyboard::KeyCode::KEY_UP_ARROW){
			auto actionRotation = RotateTo::create(0.1f, 270);
			auto action = MoveBy::create(0.2, Point(0, 32));
			
			if (checkCollision(Point(xPos, yPos + 32)) == false)
			{
				yPos += 32;
				pacman->runAction(Sequence::create(actionRotation, action, CallFunc::create(CC_CALLBACK_0(HelloWorld::checkCollisonBanana, this)), NULL));
			}
			else
				return;
		}
		if (keyCode == EventKeyboard::KeyCode::KEY_S || keyCode == EventKeyboard::KeyCode::KEY_DOWN_ARROW){
			auto actionRotation = RotateTo::create(0.1f, 90);
			auto action = MoveBy::create(0.2, Point(0, -32));
			
			if (checkCollision(Point(xPos, yPos - 32)) == false)
			{
				yPos -= 32;
				
				pacman->runAction(Sequence::create(actionRotation, action, CallFunc::create(CC_CALLBACK_0(HelloWorld::checkCollisonBanana, this)), NULL));
			}
			else
				return;
		}
		
	
	
}
void HelloWorld::checkEat(){
	//checkCollisonBanana(pos);
}
Point HelloWorld::tileCoorForPos(Point position){
	int x ,y;
	x = position.x / tmx_map->getTileSize().width;
	y = ((tmx_map->getMapSize().height*tmx_map->getTileSize().height) - position.y) / tmx_map->getTileSize().height;
	return Point(x,y);
}
bool HelloWorld::checkCollision(Point position){
	Point tileCoord = this->tileCoorForPos(position);
	int tileGid = _meta->getTileGIDAt(tileCoord);
	if (tileGid)
	{
		return true;
	}
	else{
		return false;
	}
}

void HelloWorld::checkCollisonBanana()
{

	for (auto it = vtBanana.begin(); it != vtBanana.end();)
	{
		if (std::abs((*it)->getPosition().x - xPos) <= 30 && std::abs((*it)->getPosition().y - yPos) <= 30)
		{
			
			(*it)->removeFromParent();
			it = vtBanana.erase(it);
			score += 5;
			scoreLabel->setString(std::to_string(score));
			break;
		}
		else
		{
			++it;
		}
	}
}



#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
USING_NS_CC;
class HelloWorld : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
	TMXTiledMap *tmx_map;
	TMXLayer *_meta;
	TMXObjectGroup *mainObject;
	TMXObjectGroup *foodObject;
	ValueMap spawnPoint;
	Point tileCoorForPos(Point position);
	bool checkCollision(Point position);
	//void setPlayerPosition(Point position);
    virtual bool init();
	Sprite *pacman;
	Sprite *banana;
	int xPos;
	int yPos;
	void checkEat();
	void keyPressed(EventKeyboard::KeyCode key, Event *event);
	int score;
	LabelTTF *scoreLabel;
	std::vector<Sprite*> vtBanana;
	void checkCollisonBanana();
    CREATE_FUNC(HelloWorld);
};

#endif // __HELLOWORLD_SCENE_H__
